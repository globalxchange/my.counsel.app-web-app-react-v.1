import React from 'react';
import MainLayout from '../../Layouts/MainLayout';

function MarketPlaceOne() {
  return <MainLayout active="marketplace1"></MainLayout>;
}

export default MarketPlaceOne;
