import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Login from './pages/Login/Login';
import Home from './pages/Home/Home';
import Landing from './pages/Landing/Landing';
import MarketPlaceOne from './pages/Home/MarketPlaceOne';
import ManagementOne from './pages/Home/ManagementOne';
import MyAppsOne from './pages/Home/MyAppsOne';

function App() {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/landing" component={Landing} />
      <Route exact path="/marketplace1" component={MarketPlaceOne} />
      <Route exact path="/management1" component={ManagementOne} />
      <Route exact path="/myapps1" component={MyAppsOne} />
    </Switch>
  );
}

export default App;
