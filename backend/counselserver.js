const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
// const passport = require('passport');
const logger = require('morgan')
const mongoose = require('mongoose');
const config = require('./config/database');
mongoose.connect(config.database,{ useUnifiedTopology: true,useNewUrlParser: true, useFindAndModify: false  } );
mongoose.connection.on('connected',()=>{
    console.log("Connected to database " + config.database);
})

mongoose.connection.on('error',(err)=>{
  console.log("error while connecting to database " + err)
})

const app = express();
app.use(express.static(path.join(__dirname, 'Public')));
app.use(cors());

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(logger('dev'));


app.get('/', function(req, res) {
  res.send('Welcome to Counsel App Backend');
});


// app.get('', (req, res) => {
//   res.sendFile(path.join(__dirname, './Public/index.html'));
// });

const register=require('./controllers/register.controller');

//passport
// app.use(passport.initialize());
// app.use(passport.session());

// require('./Config/Passport')(passport);

app.use('/api',register);

const port  = 3070;

app.listen(port,()=>{
    console.log("Server started on port " + port)
})


// module.exports = app;

